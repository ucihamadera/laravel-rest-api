<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'companies';

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'phone', 'country', 'address'
    ];

    public function scopeUser($query, $user)
    {
        return $query->where('user_id', $user);
    }

    public function scopeID($query, $id)
    {
        return $query->where('id', $id);
    }

    


}
