<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreJobRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:4',
            'description' => 'required|min:4',
            'skills' => 'required|min:4',
            'published_budget' => 'required|int',
            'deadline' => 'required|date_format:Y-m-d',
            'company_id' => 'required|int',
        ];
    }
}
