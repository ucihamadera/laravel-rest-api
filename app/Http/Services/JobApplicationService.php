<?php

namespace App\Http\Services;

use App\JobApplication;
use App\Company;
use App\Job;
use App\Jobseeker;
use App\Resume;
use Illuminate\Support\Facades\DB;
use App\Http\Repositories\JobApplicationRepository;
use Illuminate\Http\Request;
use App\Http\Requests\StoreJobApplication;

class JobApplicationService
{
    protected $jobapplication;

    public function __construct(JobApplicationRepository $jobapplication)
    {
        $this->jobapplication = $jobapplication;
    }

    public function validator(StoreJobApplication $request)
    {
        //check if resume_id exist
        $errors = [];
        Resume::findOrFail($request->resume_id);
        
        //check if job_id exist
        Job::findOrFail($request->job_id);
        
        //check if current_user is jobseeker | only jobseeker can post a job
        $user = auth()->user();
        if ($user->role != 'jobseeker') {
            return $errors[] = 'You have no right to post an application, only jobsekker may apply to a job posts';
        }

        //check if current user already post to the same job_id
        $is_apply = JobApplication::Resume($request->resume_id)->Job($request->job_id)->first();
        if (!empty($is_apply)) {
            $errors[] = 'You already applied for this job';
        }

        //check how many times jobseeker has applied to a job, if more than rank then return false
        $js_rank = $this->jobapplication->get_jobseeker_point($user->id);
        //user point
        $max_point = $js_rank->points;
        $current_point = $js_rank->proposal_count;

        if($current_point+2 > $max_point){
            $errors[] = 'You have reached maximum number allowed to apply jobs..';
        }

        if (count($errors) > 0) {
            return $errors;
        } else {
            return true;
        }
    }

    public function save($request){
        
        $job = Job::find($request->job_id);
        $company_id = $job->company_id;
        $user = auth()->user();

        $application = JobApplication::create([
            'intro' => $request->intro,
            'budget' => $request->budget,
            'apply_date' => $request->apply_date,
            'completion_date' => $request->completion_date,
            'resume_id' => $request->resume_id,
            'job_id' => $request->job_id,
            'company_id' => $company_id,
            'status' => false,
        ]);

        //update proposal count
        Jobseeker::User($user->id)->update(['proposal_count' => DB::raw('proposal_count+2') ]);

        return $application;
    }
}
