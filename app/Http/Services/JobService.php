<?php

namespace App\Http\Services;

use App\Job;
use App\Company;
use App\Http\Repositories\JobRepository;
use Illuminate\Http\Request;
use App\Http\Requests\StoreJobRequest;

class JobService
{
    protected $job;
    
    public function __construct(JobRepository $job)
    {
        $this->job = $job;
    }

    public function validator(StoreJobRequest $request)
    {
        $errors = [];
        //check if user has employer role
        $user = auth()->user();
        if ($user->role != 'employer') {
            $errors[] = 'You have no right to post a job, only employer are allowed..';
        }
        
        //check if company_id exists and own by employer
        $check = Company::ID($request->company_id)->User($user->id)->first();
        if(empty($check)){
            $errors[] = 'Company is not exists or you are not the owner of the company';
        }

        if(count($errors) > 0){
            return $errors;
        }

        return true;
    }
}
