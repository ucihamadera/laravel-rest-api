<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jobseeker extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'jobseekers';

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'first_name', 'last_name', 'sex', 'date_of_birth', 'phone', 'country', 'address',
    ];

    public function scopeUser($query, $user)
    {
        return $query->where('user_id', $user);
    }
    

}
