<?php

use Illuminate\Database\Seeder;
use App\Company;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        for ($i = 2; $i < 12; $i++) {
            Company::create([
                'name' => $faker->company,
                'email' => $faker->email,
                'phone' => $faker->phoneNumber,
                'country' => $faker->country,
                'address' => $faker->address,
                'user_id' => $i,
            ]);
        }
    }
}
