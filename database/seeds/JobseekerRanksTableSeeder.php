<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JobseekerRanksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jobseeker_ranks')->insert([
            [
                'name' => 'Rank A',
                'points' => 40,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Rank B',
                'points' => 20,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]
        ]);
    }
}
