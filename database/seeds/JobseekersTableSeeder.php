<?php

use Illuminate\Database\Seeder;
use App\Jobseeker;

class JobseekersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        for ($i = 12; $i < 22; $i++) {
            Jobseeker::create([
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'sex' => $faker->randomElement($array = array ('M', 'F')),
                'date_of_birth' => $faker->dateTimeThisCentury->format('Y-m-d'),
                'email' => $faker->email,
                'phone' => $faker->phoneNumber,
                'country' => $faker->country,
                'address' => $faker->address,
                'user_id' => $i,
                'rank_id' => rand(1,2),
            ]);
        }
    }
}
